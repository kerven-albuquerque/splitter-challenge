import zio._

import java.io.IOException
import scala.util.{Success, Try}

object Main extends ZIOAppDefault {
  def readPositiveInteger: ZIO[Has[Console], IOException, Int] = {
    val positiveIntegerOrNone =
      (string: String) => Try(string.toInt) match {
        case Success(x) if x > 0 => Some(x)
        case _ => None
      }

    val repeatOnError =
      Console.print("Invalid positive integer. Try again: ")
        .flatMap(_ => Console.readLine.map(positiveIntegerOrNone))
        .repeatUntil(_.isDefined)
        .map(_.get)

    Console.print("Enter a positive integer as the character limit: ")
      .flatMap(_ => Console.readLine.map(positiveIntegerOrNone))
      .someOrElseZIO(repeatOnError)
  }

  def run: ZIO[Has[Console], IOException, Unit] =
    for {
      _ <- Console.printLine("This script breaks a text into lines with a limit of characters.")
      _ <- Console.printLine("It does not break words. Small limits or long words may generate overflows.")
      _ <- Console.printLine("")
      charLimit <- readPositiveInteger
      _ <- Console.print("Enter a line to be split: ")
      line <- Console.readLine
      lines = Splitter.splitLines(line, charLimit)
      _ <- Console.printLine("")
      _ <- Console.printLine("Result:")
      _ <- ZIO.collectAll(lines.map(x => Console.printLine(x)))
    } yield ()
}
