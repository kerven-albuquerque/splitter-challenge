object Splitter {
  def appendWordToParagraph(charLimit: Int)(paragraph: Seq[String], word: String): Seq[String] =
    if (paragraph.isEmpty || paragraph.last.length + word.length >= charLimit)
      paragraph :+ word
    else
      paragraph.init :+ s"${paragraph.last} $word"

  def splitLines(input: String, charLimit: Int = 80): Seq[String] =
    input.split("\\s")
      .filter(_.nonEmpty)
      .foldLeft(Seq.empty[String])(appendWordToParagraph(charLimit))
}
