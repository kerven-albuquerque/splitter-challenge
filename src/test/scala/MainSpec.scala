import org.specs2.mutable.Specification
import zio._

import java.io.IOException

object MainSpec extends Specification {

  case class TestConsole(var input: Seq[String] = Seq.empty[String]) extends Console {
    var output = Seq.empty[String]
    var error = Seq.empty[String]

    override def print(line: => Any)(implicit trace: ZTraceElement): IO[IOException, Unit] =
      if (output.isEmpty) printLine(line) else IO.succeed { output = output.init :+ s"${output.last}$line" }

    override def printError(line: => Any)(implicit trace: ZTraceElement): IO[IOException, Unit] =
      if (error.isEmpty) printLineError(line) else IO.succeed { error = error.init :+ s"${error.last}$line" }

    override def printLine(line: => Any)(implicit trace: ZTraceElement): IO[IOException, Unit] =
      IO.succeed { output = output :+ line.toString }

    override def printLineError(line: => Any)(implicit trace: ZTraceElement): IO[IOException, Unit] =
      IO.succeed { error = error :+ line.toString }

    override def readLine(implicit trace: ZTraceElement): IO[IOException, String] =
      IO.succeed { val ans = input.head; input = input.tail; ans }
  }

  "readPositiveInteger" >> {
    "must return a positive Int, if entered" >> {
      val console: Console = TestConsole(Seq("10"))
      val runtime = Runtime(Has(console), RuntimeConfig.default)
      val ans = runtime.unsafeRun(Main.readPositiveInteger)
      ans must beEqualTo(10)
    }
    "must keep asking until a positive Int is provided" >> {
      val console: Console = TestConsole(Seq("-1", "abc", "0", "-2", "10", "20"))
      val runtime = Runtime(Has(console), RuntimeConfig.default)
      val ans = runtime.unsafeRun(Main.readPositiveInteger)
      ans must beEqualTo(10)
    }
  }

  "run" >> {
    "must read a limit and a line and print it split" >> {
      val console = TestConsole(Seq("10", "This is a sentence."))
      val runtime: Runtime[Has[Console]] = Runtime(Has(console), RuntimeConfig.default)
      runtime.unsafeRun(Main.run)
      console.error must haveLength(0)
      val output = console.output.takeRight(2)
      val expected = Seq("This is a", "sentence.")
      output must beEqualTo(expected)
    }
  }
}
