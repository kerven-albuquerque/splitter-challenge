import org.specs2.mutable.Specification

object SplitterSpec extends Specification {

  val word = "Word"
  val charLimit = 10
  val completeLine = "Ten chars."
  val almostCompleteLine = "8 chars."
  val emptyLine = "Empty"

  "appendWordToParagraph" >> {
    "must return a paragraph with a new line" >> {
      "if the paragraph is empty" >> {
        val par = Splitter.appendWordToParagraph(charLimit)(Seq.empty, word)
        par must haveLength(1)
        par.last must beEqualTo(word)
      }
      "if the last line is complete" >> {
        val oldPar = Seq(completeLine)
        val par = Splitter.appendWordToParagraph(charLimit)(oldPar, word)
        par must haveLength(2)
        par.last must beEqualTo(word)
      }
      "if the word would cause an overflow on the last line" >> {
        val oldPar = Seq(almostCompleteLine)
        val par = Splitter.appendWordToParagraph(charLimit)(oldPar, word)
        par must haveLength(2)
        par.last must beEqualTo(word)
      }
    }
    "must return a paragraph with a new word on its last line" >> {
      "if the word can be added without overflow" >> {
        val oldPar = Seq(emptyLine)
        val par = Splitter.appendWordToParagraph(charLimit)(oldPar, word)
        par must haveLength(1)
        par.last must beEqualTo(s"$emptyLine $word")
      }
    }
  }

  "splitLines" >> {
    "must split the lines as expected" >> {
      val input = "In 1991, while studying computer science at University of Helsinki, Linus Torvalds began a project that later became the Linux kernel. He wrote the program specifically for the hardware he was using and independent of an operating system because he wanted to use the functions of his new PC with an 80386 processor. Development was done on MINIX using the GNU C Compiler."
      val expected = Seq(
        "In 1991, while studying computer science",
        "at University of Helsinki, Linus",
        "Torvalds began a project that later",
        "became the Linux kernel. He wrote the",
        "program specifically for the hardware he",
        "was using and independent of an",
        "operating system because he wanted to",
        "use the functions of his new PC with an",
        "80386 processor. Development was done on",
        "MINIX using the GNU C Compiler."
      )
      val ans = Splitter.splitLines(input, 40)
      ans must beEqualTo(expected)
    }
    "must overflow without errors, if the char limit is too small" >> {
      val input = "This is a sentence."
      val expected = Seq("This", "is", "a", "sentence.")
      val ans = Splitter.splitLines(input, 1)
      ans must beEqualTo(expected)
    }
  }
}
