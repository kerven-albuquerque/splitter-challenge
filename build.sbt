ThisBuild / scalaVersion     := "2.12.10"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "splitter-challenge",
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio" % "2.0.0-M4",
      "dev.zio" %% "zio-streams" % "2.0.0-M4",
      "org.specs2" %% "specs2-core" % "4.16.1" % "test"
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
